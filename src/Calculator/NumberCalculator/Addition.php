<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 3/21/2018
 * Time: 8:38 PM
 */

namespace Pondit\Calculator\NumberCalculator;


class Addition
{
    public $firstnumber;
    public $secondnumber;

    public function __construct($number1, $number2)
    {
        $this->firstnumber = $number1;
        $this->secondnumber = $number2;
    }

    public function addtion()
    {
        $total = $this->firstnumber + $this->secondnumber;
        return $total;
    }
}